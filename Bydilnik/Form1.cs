﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bydilnik
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.BackColor = Color.Black;
            this.StartPosition = FormStartPosition.CenterScreen;
            labelTime.ForeColor = Color.Green;
            label1.ForeColor = Color.White;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int hours = DateTime.Now.Hour;
            int min = DateTime.Now.Minute;
            int sec = DateTime.Now.Second;

            labelTime.Text = hours.ToString("00") + ":" + min.ToString("00")+":"+sec.ToString("00");          

            if(listBox1.Items.Contains(labelTime.Text))
            {
                MessageBox.Show("Вставай)");
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            timer1.Start();
            buttonReset.Enabled = true;
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            buttonReset.Enabled = false;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {           
            string hour = numericUpDownHour.Value.ToString("00");
            string minute = numericUpDownMinute.Value.ToString("00");

            listBox1.Items.Add(hour+":"+minute+":"+"00");

            numericUpDownHour.Value = 0;
            numericUpDownMinute.Value = 0;
        }

        private void buttonRemoveAt_Click(object sender, EventArgs e)
        {
            object item = listBox1.SelectedItem;

            if(item==null)
            {
                MessageBox.Show("Ошибка! Не выбран будильник.");
            }

            listBox1.Items.Remove(item);
            listBox1.SelectedItem = null;
        }

        private void buttonCleanAll_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
        }

        private void numericUpDownHour_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownHour.Maximum = 23;
        }

        private void numericUpDownMinute_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownMinute.Maximum = 60;
        }
    }
}
